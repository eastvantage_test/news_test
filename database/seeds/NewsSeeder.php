<?php

use Illuminate\Database\Seeder;


class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news')->insert([
            'title' => 'Sample Title',
            'description' => 'Sample Description',
            'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
