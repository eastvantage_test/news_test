<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function create(Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        $isInsert = DB::table('news')
        ->insert(
            [
                'title' => $request->title, 
                'description' => $request->description,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
        );

        $check = $isInsert ? 'success' : 'error';
        return response()->json($check);
    }

    public function show() {
        $data['news'] = DB::table('news')
                        ->orderBy('created_at', 'desc')
                        ->get();
        return response()->json($data);
    }
}
